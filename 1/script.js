document.querySelector('.message__btn').addEventListener('click', function () {
    document.querySelector('.message').style.display = 'none';
});


let tabs = document.querySelectorAll('.tab');
let tab_contents = document.querySelectorAll('.tab-content');

function DelClass() {

    tabs.forEach(function(tab) {
        tab.classList.remove('tab_active');
    });

    tab_contents.forEach(function(tab_content) {
        tab_content.classList.remove('tab-content_show');
    });

};

tabs.forEach(function(tab) {
    tab.addEventListener('click', function (element) {

        DelClass();

        tab.classList.add('tab_active');
        document.querySelector('.tab-content[data-tab="' + element.target.getAttribute('data-tab') + '"]').classList.add('tab-content_show');
      
    });
});
