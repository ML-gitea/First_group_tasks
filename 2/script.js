window.steps = [];
let steps_item = document.querySelectorAll('.steps-item');
let i = 0;

window.onload = steps_item.forEach(function(item) {
    window.steps[i] = item.outerHTML;
    i++;
});

document.querySelector('.steps__button').addEventListener('click', function() {
    
    console.log('Обработчик события click');
    let number = prompt('Введите номер желаемого пункта.');
    let filter = /^[0-9]+$/g
    
    if (filter.test(number)) {
        const event = new CustomEvent('steps:check-item', {detail: {id: number - 1}});
        document.dispatchEvent(event);
    } else {
        alert('Ошибка! Введенно не верное значение!');
    };

});

document.addEventListener('steps:check-item', function(event) {

    let id = event.detail.id;

    if (0 <= id && id < steps.length) {

        let div = document.createElement('div');
        div.innerHTML = window.steps[id];
        let element = div.firstChild;

        element.classList.add('steps-item_active');
        steps_item[id].outerHTML = element.outerHTML;

    } else {
        let N = id + 1;
        alert('Не найден пункт с номером ' + N);
    };

});
